\chapter{\ZnPdf}
\label{sec:znpd}

En este capitulo se estudia la familia de compuestos \ZnPdf.  Estos
compuestos, a diferencia de los estudiados en los cap�tulos
anteriores, son de tipo \emph{composite}. 

A pesar de ello, ha sido posible encontrar un modelo estructural �nico
para los diferentes miembros de la familia.  En este caso, el valor
del vector de modulaci�n debe obtenerse de la periodicidad de los dos
subsistemas, que forman el composite (ver
secci�n~\ref{sec:composite}), y que se muestran claramente el diagrama
de difracci�n.

En la secci�n~\ref{sec:znpd-descripcion} se presenta una descripci�n
3-dimensional de varios miembros de la familia de compuestos \ZnPdf.
En la siguiente secci�n (\ref{sec:znpd-modelo}) se propone un modelo
superespacial que unifica la descripci�n de los distintos miembros de
la familia.  En la secci�n~\ref{sec:znpdf-clusters} se presentan los
compuestos analizados usando una descripci�n basada en agregados
at�micos, para pasar a describir la familia de compuestos como
aproximantes a un cuasicristal unidimensional
(secci�n~\ref{sec:znpd-aproximantes}).  Cierra el capitulo una secci�n
(\ref{sec:znpd-conclusiones}) donde se resumen las conclusiones
obtenidas de este an�lisis.


\section{Descripci�n de la familia \ZnPdf}
\label{sec:znpd-descripcion}

Varios compuestos del sistema \ZnPdf en el rango de composici�n
$0.14<x<0.25$ han sido sintetizados y analizados
\citep{Gourdon2006b,Gourdon2006a}.  En la tabla~\ref{tab:znpd-celdas}
se muestran los par�metros de red de seis de ellos y grupos espaciales
de las dos estructuras resueltas mediante modelos tridimensionales.
Todos los compuestos tiene simetr�a ortorr�mbica con un centrado $F$ o
$C$, dependiendo de la fase concreta, excepto el compuesto
\ZnPd{81.9}{18.1} que presenta simetr�a c�bica $I\bar{4}3m$.  Para
esta fase c�bica existe, tambi�n, una descripci�n ortorr�mbica
consistente con los otros compuestos de la familia; que se obtiene
mediante la transformaci�n
\begin{equation}
  \vt{a}'\rightarrow \vt{b}\quad \vt{b}'\rightarrow \vt{a}-\vt{c}\quad
  \vt{c}'\rightarrow \vt{a}+\vt{c}.
\end{equation}

\begin{table}
  \centering
  \begin{tabular}{lcccll}
    \toprule
    Composici�n & \multicolumn{3}{c}{Par�metros de red} & &
    $m/n$ \\
    & $a$ & $b$ & $c$ \\
    \midrule
    \ZnPd{81.9}{18.1} & $9.0906(3)$ & $12.912(6)$ & $12.912(6)$
    &$F2mm$  &$5/3$\\
    (fase c�bica) & $9.0906(3)$ & & & $I\bar{4}3m$\\
    \ZnPd{79}{21} & $9.0901(6)$ & $12.919(8)$ & $106.86(6)$ & &$41/25$\\
    \ZnPd{78}{22} & $9.115(3)$ & $12.909(5)$ & $46.894(5)$ & &$18/11$\\
    \ZnPd{77.5}{22.5} & $9.120(9)$ & $12.927(7)$ & $80.98(5)$ & &$31/15$\\
    \ZnPd{77}{23} & $9.114(3)$ & $12.915(4)$ & $146.10(13)$ & &$57/35$\\
    \ZnPd{212}{64} & $9.112(4)$ & $12.929(3)$ & $33.32(1)$ & $Ccme$ &$13/8$\\
    \bottomrule
  \end{tabular}
  \caption[Par�metros de red de las seis estructuras estudiadas en
  \citet{Gourdon2006a}.]{Par�metros de red de las seis estructuras
    estudiadas en   \citet{Gourdon2006a}. Se muestra el grupo espacial
    de las dos     estructuras resueltas. $n$ y $m$ son el n�mero de
    �tomos a lo    largo de las dos cadenas de �tomos que se muestran
    en la    figura~\ref{fig:znpd-bloques}}. 
  \label{tab:znpd-celdas}
\end{table}

En la descripci�n ortorr�mbica los par�metros de red son
$a\sim\unit{9.1}{\angstrom}$, $b\sim\unit{12.9}{\angstrom}$
y~$c\sim\unit{4.3}{\angstrom}\times n$, donde $n$ representa el n�mero
de �tomos de \Pd a lo largo del eje $c$. (ver
figura~\ref{fig:znpd-bloques}).

De estas seis fases, \ZnPd{81.9}{18.1} y \ZnPd{75.8}{24.2} han sido
resueltas y refinadas en $3D$ \citep{Gourdon2006b}.  A lo largo del
eje $c$ se distinguen dos cadenas (representadas por lineas gruesas en
la figura~\ref{fig:znpd-bloques}) de �tomos con distinta periodicidad.
La primera cadena est� compuesta por �tomos de \Zn (periodicidad $5$
para \ZnPd{81.9}{18.1} y $13$ para \ZnPd{75.8}{24.2}) y la segunda
cadena est� compuesta principalmente por �tomos de \Pd (periodicidad
$3$ para \ZnPd{81.9}{18.1} y $8$ para \ZnPd{75.8}{24.2}). Estas dos
cadenas de �tomos son los dos subsistemas que forman el
\foreign{composite}.

La existencia de dos subsistemas con distinta periodicidad queda
patente en el diagrama de difracci�n (ver figura~1 de
\citet{Gourdon2006b}). Las reflexiones observadas pueden indexarse
utilizando dos conjuntos de vectores rec�procos
$\{\rec{a},\rec{b},\rec{c_1}\}$ y $\{\rec{a},\rec{b},\rec{c_2}\}$.
Los vectores rec�procos relacionados con los vectores de base \vt{a}
y~\vt{b} son los mismos en los dos subsistemas. Los vectores de red
paralelos a \vt{c} para cada subsistema son $\vt{c_1} \sim 12.912/5
\sim 33.32/13 \sim \unit{2.57}{\angstrom}$ y~$\vt{c_2} \sim 12.912/3
\sim 33.32/8 \sim \unit{4.23}{\angstrom}$ y corresponden a las
distancias entre los �tomos de la primera y de la segunda cadena,
respectivamente.

Las otras cuatro estructuras no han podido ser refinadas debido al
gran tama�o de la celda unidad (ver tabla~\ref{tab:znpd-celdas}).
Estos compuestos pueden ser interpretados en este contexto a partir de
las dos estructuras de referencia como estructuras tipo
\foreign{intergrowth}; es decir, pueden describirse como apilamientos
intercalados de los miembros de referencia de la familia (ver
secci�n~\ref{sec:znpd-aproximantes}).  Para ello se ha desarrollado un
modelo superespacial que permite estudiar todos los miembros de la
familia.


\section{Modelo superespacial para la familia de compuestos \ZnPdf}
\label{sec:znpd-modelo}

\lineamenos
Los compuestos \ZnPd{81.9}{18.1} y~\ZnPd{75.8}{24.2} han sido
analizado como estructuras \emph{composite} (ver secci�n
\ref{sec:composite}). A partir de los refinamientos de \ZnPd{212}{64}
en $3D$ \citep{Gourdonpri}, y usando como vector de modulaci�n \vt{q},
\begin{equation}
  \label{eq:znpd-q}
  \vt{q} = n/m\rec{c},
\end{equation}
\lineamenos
donde $n$ y $m$ son el n�mero de �tomos en cada una de las cadenas que
forman los dos subsistemas descritos en la
secci�n~\ref{sec:znpd-descripcion} (ver
figura~\ref{fig:znpd-bloques}), se han reconstruido los \DA{}s que
forman el modelo estructural de la familia. En la
figura~\ref{fig:znpd-modelo} se muestra un esquema del modelo
superespacial para el compuesto \ZnPd{212}{64} obtenido de los datos
$3D$.

\begin{figure}
  \centering
  \includegraphics[width=0.36\textwidth]{znpd-model-1.pdf}
  \caption[\foreign{Embedding} de la estructura \ZnPd{212}{64} en el
  superespacio.]{\foreign{Embedding} de la estructura \ZnPd{212}{64}
    en el superespacio. Los c�rculos grandes pertenecen al \DA del
    primer subsistema. Los c�rculos peque�os pertenecen a los \DA
    incluidos en el segundo subsistema. Los c�rculos rojos representan
    �tomos de \Zn, los verdes a �tomos de \Pd y los azules a
    posiciones mixtas de \Zn y~\Pd.}
\label{fig:znpd-modelo}
\end{figure}


La celda unidad promedio tiene unos par�metros de red\footnote{Se han
  intercambiado los ejes \vt{a} y~\vt{b} respecto a la base utilizada
  en \citet{Gourdon2006a} para hacerla conincidir con la utilizada en
  la tabla~\ref{tab:znpd-celdas}.}  $a=\unit{8.869}{\angstrom}$,
$b=\unit{12.929}{\angstrom}$ y $c=\unit{2.5631}{\angstrom}$. La
estructura est� compuesta de un \DA en el primer subsistema y tres en
el segundo.  La matriz $W$ \citep{smaalen1991} que relaciona los
dos subsistemas es
\begin{equation}
  \label{eq:znpd-w}
  W=
  \begin{pmatrix}
    1&0&0&0\\
    0&1&0&0\\
    0&0&0&1\\
    0&0&1&0
  \end{pmatrix}.
\end{equation}

El primer subsistema contiene �nicamente un \DA de \Zn, mientras que el
segundo subsistema esta formado por un \DA de \Zn, un \DA de \Pd y un
tercer \DA mixto, denominado $\mathrm{M}1$, de \Zn y \Pd.  Las
coordenadas en la celda promedio y las restricciones de la modulaci�n
impuestas por la simetr�a para cada \DA se muestran en la
tabla~\ref{tab:znpd-dominios}.

  \begin{table}
   \centering
    \begin{tabular}{llcccc}
      \toprule
      �tomo & Subsistema & $x$ & $y$ & $z$ &  Modulaci�n displaciva\\
      \midrule
      $\Zn1$ & $1$ & $0$ & $0$ & $0$ & $(A_{2n+1},0,A_{2n})$\\
      $\Zn2$ & $2$ & $\nicefrac{1}{8}$ & $\nicefrac{1}{4}$ & $\nicefrac{3}{4}$ &$(B_{2n+1},B_{2n},A_{2n})$ \\
      $\Pd1$ & $2$ & $0$ & $\nicecuarto$ & $\nicecuarto$ &$(B_{2n+1},0,A_{2n})$ \\
      $\mathrm{M}1$ & $2$ & $\nicefrac{1}{6}$ & $\sim0.6$ & $\nicefrac{3}{4}$ & $(B_n,B_n,A_n)$ \\
      \bottomrule
    \end{tabular}
    \caption[Par�metros estructurales de la descripci�n superespacial
    del compuesto \ZnPd{212}{64}.]{Par�metros estructurales de la
      descripci�n superespacial del compuesto \ZnPd{212}{64}. En la
      columna de la modulaci�n displaciva se muestran los t�rminos no
      nulos de la expansi�n $u_i(x_4) = \sum_{n=0}^k A_{n}\sin2\pi n
      x_4+\sum_{n=0}^kB_{n}\cos2\pi n x_4$, con $i=1,2,3$.}
    \label{tab:znpd-dominios}
  \end{table}



\begin{table}
  \centering
  \begin{tabular}{llll}
    \toprule
    $(\nicemedio,\nicemedio,0,0)+$ &
    $(\nicemedio,0,\nicemedio,\nicemedio)+$\\
    \multicolumn{2}{c}{$(0,\nicemedio,\nicemedio,\nicemedio)+$}\\
    \midrule
    $x_1,x_2,x_3,x_4$ & $-x_1,-x_2,-x_3,-x_4$\\
    $-x_1,x_2,x_3,\nicemedio+x_4$ & $x_1,-x_2,-x_3,\nicemedio-x_4$\\
    $x_1,-x_2,x_3,x_4$ & $-x_1,x_2,-x_3,-x_4$\\
    $-x_1,-x_2,x_3,\nicemedio+x_4$ & $x_1,x_2,-x_3,\nicemedio-x_4$\\
    \bottomrule
  \end{tabular}
  \caption{Elementos del grupo superespacial $Xmmm(00\gamma)s00$}
  \label{tab:znpd-simetria}
\end{table}

El grupo superespacial de simetr�a es $Xmmm(00\gamma)s00$ y sus
elementos de simetr�a se listan en la tabla~\ref{tab:znpd-simetria}.
Los grupos espaciales de simetr�a $3D$ para los casos conmensurables
se han calculado utilizando el m�todo descrito en la
secci�n~\ref{seccion_grupos_espaciales} y se muestran en la
tabla~\ref{tab:znpd-3d}.  La simetr�a de las fases conocidas
($\gamma=3/5$ y $\gamma=8/13$), mostradas en la
tabla~\ref{tab:znpd-celdas}, coinciden con los cortes $t=1/4s$ del
caso $r,s=\text{impar}$, y el corte $t=0$ del caso
$r=\text{par}$ de la tabla~\ref{tab:znpd-3d}, respectivamente.

\begin{table}
  \centering
  \begin{tabular}{llll}
    \toprule
    & $t=0\mod1/2s$ & $t=1/4s\mod1/2s$ & $t\text{ arbitrario}$\\
    \midrule
    $s\text{ par}$ & $Ccmm$ & $Ccme$ & $Ccm2_1$\\
    $r\text{ par}$ & $Ccme$ & $Ccmm$ & $Ccm2_1$\\
    $r,s\text{ impar}$ & $F1\nicefrac{2}{m}1$ & $F2mm$ & $F1m1$\\
    \bottomrule
  \end{tabular}
  \caption{Grupos tridimensionales resultantes del grupo superespacial
  $Xmmm(00\gamma)s00$ para casos conmensurables $\gamma=r/s$.}
  \label{tab:znpd-3d}
\end{table}

La aplicaci�n de este modelo superespacial ser� llevado a cabo en el
futuro.  


\section{La familia \ZnPdf como apilamiento de agregados at�micos}
\label{sec:znpdf-clusters}


Es posible hacer una descripci�n de las dos estructuras
\ZnPd{81.9}{18.1} y~\ZnPd{75.8}{24.2} usando \foreign{clusters}
(agregados) de �tomos.  Las estructuras est�n formadas por tres
\foreign{clusters}, con la siguiente forma y composici�n (ver
figura~\ref{fig:znpd-bloques}):
\begin{enumerate}
\item Icosaedro doble $(D1)$  con composici�n $\Pd_2\Zn_{21}$.
\item Icosaedro triple $(T1)$ con composici�n $\Pd_3\{Zn_{29}\Pd\}$.
\item Cuasi-icosaedro $(Q1)$ con composici�n $(\Zn,\Pd)\Zn_9\Pd_24$.
\end{enumerate}
Utilizando estos poliedros el compuesto \ZnPd{81.9}{18.1} esta
formado por la secuencia $Q1D1Q1D1\cdots$ y \ZnPd{75.8}{24.2} por la
cadena $Q1T1Q1T1\cdots$ \citep{Gourdon2006a}.

\begin{sidewaysfigure}
  \centering
  \includegraphics[width=\textwidth]{znpd-1.pdf}
  \caption[Esquema de las periodicidades de los subsistemas de las
  estructuras \ZnPdf.]{Esquema de las periodicidades de los
    subsistemas de las estructuras \ZnPdf.  Tambi�n se muestra los
    esquemas de los conglomerados de �tomos $D1$, $T1$ y~$Q1$ (ver
    texto) que forman las estructuras \ZnPd{81.9}{18.9} (arriba) y
    \ZnPd{75.8}{24.2} (abajo) (Figura publicada en
    \citep{Gourdon2006b}).}
  \label{fig:znpd-bloques}
\end{sidewaysfigure}


\section{An�lisis de las estructuras como aproximantes a un
  cuasicristal unidimensional}
\label{sec:znpd-aproximantes}

Es posible hacer una analog�a entre las estructuras analizadas y la
secuencia de Fibonacci (ver secci�n~\ref{sec:cuasicristales}), si se
define $D1$ como el segmento corto $S$ y $T1$ como el segmento largo
$L$ ($Q1$ es com�n a los dos compuestos y hace de separador entre $D1$
y $T1$). De esta forma, \ZnPd{81.9}{18.1} y \ZnPd{75.8}{24.2} se
representan como las secuencias $SS\ldots$ y $LL\dots$ (ver la
figura~\ref{fig:znpd-farey}) respectivamente.  


Como se ha indicado en la introducci�n, un cuasicristal es una
estructura cuasiperi�dica cuyo diagrama de difracci�n es discreto
(indicaci�n de que posee orden de largo alcance), y cuyo grupo puntual
es incompatible con la periodicidad. Localmente, est� formado por un
n�mero relativamente peque�o de agregados at�micos diferentes,
dispuestos de forma cuasiperi�dica. Un cuasicristal, por tanto, tiene
periodicidad infinita. Sin embargo, puede construirse una estructura
peri�dica utilizando los mismos agregados at�micos que forman el
cuasicristal hasta una distancia $\ell$ (en las tres direcciones del
espacio), mucho mayor que el tama�o de los agregados at�micos, para
formar la celda unidad. Esta celda unidad de gran tama�o se repite
hasta el infinito. Esta estructura, peri�dica, es similar, localmente,
al cuasicristal y recibe el nombre de aproximante. Cuanto mayor sea la
celda unidad del aproximante (cuanto mayor sea $\ell$), m�s parecida ser�
esta estructura a la del cuasicristal. Existe evidencia experimental
de la existencia de aproximantes para algunos cuasicristales
icosah�dricos (\cite{Janot1992}).

En el caso de la cadena de Fibonacci de la
figura~\ref{figura_superespacio_Fibonacci}, el \DA que representa a
los �tomos no es m�s que la proyecci�n sobre el espacio interno de la
celda unidad cuadrada en el superespacio. La longitud de la proyecci�n
de los vectores de red sobre el espacio interno es $1$ y la raz�n
�urea $\phi=(\sqrt{5}-1)/2 = 0.618034\dots$, respectivamente. Como la
raz�n �urea es un n�mero irracional, la secci�n horizontal de la
figura~\ref{figura_superespacio_Fibonacci} que pasa por el origen, no
pasa por ning�n otro nodo de la red y, por tanto, la estructura tiene
per�odo infinito. Sin embargo, si se sustituye ese n�mero por una de
sus aproximaciones racionales ($1/1$, $1/2$, $2/3$, $3/5$,$5/8$,
\dots) la secci�n pasa por otros nodos de la red, y la estructura
resultante ser� peri�dica. La serie de n�meros mencionada es la serie
de Fibonacci, cuyo primer t�rmino es $F_1=1$ y el t�rmino de orden $n$
es $F_n=1/(1+F_{n-1})$. El l�mite para $n=\infty$ es la raz�n �urea.
Si en la cadena de Fibonacci se utiliza el aproximante $n/m$ de la
raz�n �urea, cuanto mayor sea el valor de $m$, mayor ser� la celda
unidad de la estructura peri�dica resultante, y mayor ser� la
similitud (localmente) entre la estructura peri�dica y la cadena de
Fibonacci.  Una forma alternativa de obtener los aproximantes
sucesivos es la siguiente:

Partiendo del aproximante m�s sencillo $1/1$, que contiene un �nico
intervalo at�mico S en la celda unidad,
\begin{equation}
  \label{eq:aproximante1}
  SSSS\ldots,
\end{equation}
y aplicar de forma iterativa la transformaci�n
\begin{equation}
  \label{eq:transformacion}
  S\rightarrow L\qquad L\rightarrow LS.
\end{equation}

Aplicando la transformaci�n~(\ref{eq:transformacion}), del primer aproximante
(\ref{eq:aproximante1}) se obtiene
\begin{equation}
  \label{eq:aproximante2}
  LLLL\ldots.
\end{equation}
La aplicaci�n sucesiva de la transformaci�n da lugar a los aproximantes
\begin{equation}
  \label{eq:aproximante3}
  LSLSLS\ldots\rightarrow LSLLSLLSL\ldots\rightarrow LSLLSLSLLSLSLLS\ldots
\end{equation}
La periodicidad de estos aproximantes es $S$, $L$, $LS$, $LSL$
y~$LSLLS$, respectivamente. A medida que el periodo aumenta, la
relaci�n entre el n�mero de periodos $L$ y $S$ forma la serie $1/1$,
$2/1$, $3/2$, $5/3$, $8/5$,\..., que es la serie de aproximaciones
racionales al n�mero �ureo $\tau=1/\phi=1.618034\dots$.

Los dos compuestos \ZnPd{81.9}{18.1} y \ZnPd{75.8}{24.2} pueden
interpretarse como los aproximante $1/1$ y $2/1$ de un cuasicristal
unidimensional.  Estos dos compuestos son las fases de referencia de
las cuales es posible derivar la estructura de los dem�s compuestos
utilizando el \emph{�rbol de Farey} \citep{perezmato1999} (ver
figura~\ref{fig:znpd-farey}). Las estructuras de referencia forman la
primera generaci�n del �rbol de Farey. 


\begin{figure}
  \centering
\subfloat[]{\label{fig:znpd-farey-a}
    \includegraphics[width=0.9\textwidth]{znpd-2a.pdf}
}

\subfloat[]{\label{fig:znpd-farey-b}

    \includegraphics[width=0.7\textwidth]{znpd-2bc.pdf}
}
  \caption[Construcci�n del �rbol de Farey para la familia
  \ZnPdf.]{\subref{fig:znpd-farey-a} �rbol de Farey de las distintas
    estructuras hipot�ticas que pueden obtenerse a partir de las dos
    estructuras \emph{padre} (ver texto). \subref{fig:znpd-farey-b}
    Estructura hipot�tica para las combinaciones $42/25$ y
    $57/35$ (Figura publicada en \citep{Gourdon2006b}).}
  \label{fig:znpd-farey}
\end{figure}

Cada compuesto $m/n$ es la yuxtaposici�n de los compuestos asociados a
los cocientes   $m'/n'$
y~$m''/n''$ de forma que cumplen la siguiente relaci�n:
\begin{equation}
  \frac{m}{n} = \frac{m'}{n'} \oplus \frac{m''}{n''} \equiv \frac{m'+m''}{n'+n''}.
\end{equation}
Por ejemplo, el miembro $n/m=18/11$ (la segunda generaci�n) se obtiene
uniendo a lo largo de la direcci�n $c$ las estructuras de de la
primera generaci�n $5/3$ y $13/8$.  Uniendo la estructura de la
segunda generaci�n con las de la primera se obtiene las dos
estructuras de la tercera generaci�n.  De esta manera, todos los
miembros del �rbol son aproximantes de un cuasicristal unidimensional
que se obtendr�a en el caso $m/n\to\tau$, puesto que el �rbol de Farey
converge a $\tau$ cuando las fracciones de las que parte est�n
formados por fracciones de la secuencia de Fibonacci ($5/3 = F_5/F_4$
y~$13/8=F_7/F_6$).

\section{Conclusiones}
\label{sec:znpd-conclusiones}

Este trabajo muestra que es posible analizar la familia de compuestos
\ZnPdf bajo un �nico modelo superespacial. De este estudio pueden
obtenerse las siguientes conclusiones:

\begin{itemize}

\item Se ha propuesto un modelo superespacial para para analizar las
  estructuras previamente examinadas mediante un modelo 3-dimensional.
\item Este modelo superespacial puede ser utilizado como punto de
  partida para obtener la estructura de otros miembros de la familia,
  que por su tama�o de celda grande no pueden analizarse mediante
  m�todos de cristalograf�a convencional.
\item Del an�lisis realizado en este cap�tulo se deduce que al menos
  en este caso, un cuasicristal unidimensional puede interpretarse
  como el l�mite inconmensurable de un conjunto de estructuras
  \emph{intergrowth} de composici�n conmensurable (aproximantes del
  cuasicristal), con dominios at�micos continuos.
\end{itemize}
