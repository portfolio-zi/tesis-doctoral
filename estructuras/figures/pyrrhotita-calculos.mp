verbatimtex
%&latex
\documentclass{tesis}
\begin{document}
etex


input mathematical;
input graph;

beginfig(1);
picture opencircle, closecircle;

 draw fullcircle scaled 7pt;
 opencircle := currentpicture;
 currentpicture := nullpicture;

 fill  fullcircle scaled 7pt;
 closecircle := currentpicture;
 currentpicture := nullpicture;


draw begingraph(3in,2in);
    glabel.lft(btex $u_x$ etex,OUT);
    glabel.bot(btex $t$ etex,OUT);
    setrange(0.125,-0.2,1.125,0.4);
    numeric currentlabeloffset;
    currentlabeloffset := labeloffset;
    labeloffset := 0pt;
    gdata("Feu_ab-initio_1-4.dat",s,
      glabel(closecircle,((scantokens s1)+0.125,(scantokens s2)*6.9));
      );
    gdata("Feu_exp_1-4.dat",s,
      glabel(opencircle,((scantokens s1),(scantokens s2)*6.9));
      );
    labeloffset := currentlabeloffset;
    
    vardef f(expr x) =
      numeric result;
      result = 6.9*0.0001*(57+372*cos(2*pi*(x-0.125))+73*cos(4*pi*(x-0.125)));
      result
    enddef;

    path p;
    p := (0.1875,f(0.1875))
    for i= 0.1875 step 0.01 until 1.0625:
       ..(i,f(i))
    endfor;
    gdraw p;
    autogrid(,);
    otick.bot(btex $\scriptstyle 1/8$ etex,1/8);
    otick.bot(btex $\scriptstyle 3/8$ etex,3/8);
    otick.bot(btex $\scriptstyle 5/8$ etex,5/8);
    otick.bot(btex $\scriptstyle 7/8$ etex,7/8);
    otick.bot(btex $\scriptstyle 9/8$ etex,9/8);
    otick.lft(btex $\scriptstyle -0.20$ etex,-0.20);
    otick.lft(btex $\scriptstyle -0.10$ etex,-0.10);
    otick.lft(btex $\scriptstyle 0.00$ etex,0.0);
    otick.lft(btex $\scriptstyle 0.10$ etex,0.10);
    otick.lft(btex $\scriptstyle 0.20$ etex,0.20);
    otick.lft(btex $\scriptstyle 0.40$ etex,0.40);
    otick.lft(btex $\scriptstyle 0.30$ etex,0.30);
  endgraph;
endfig;

beginfig(2);
draw begingraph(3in,2in);
    glabel.lft(btex $u_x$ etex,OUT);
    glabel.bot(btex $t$ etex,OUT);
    setrange(0.125,-0.3,1.125,0.3);
    numeric currentlabeloffset;
    currentlabeloffset := labeloffset;
    labeloffset := 0pt;
    gdata("Feu_ab-initio_1-4.dat",s,
      glabel(closecircle,((scantokens s1)+0.125,(scantokens s3)*12.0));
      );
    gdata("Feu_exp_1-4.dat",s,
      glabel(opencircle,((scantokens s1),(scantokens s3)*12.0));
      );
    labeloffset := currentlabeloffset;
    
    vardef f(expr x) =
      numeric result;
      result = -12.0*0.0001*(87*sin(2*pi*(x-0.125))-139*sin(4*pi*(x-0.125)));
      result
    enddef;

    path p;
    p := (0.1875,f(0.1875))
    for i= 0.1875 step 0.01 until 1.0625:
       ..(i,f(i))
    endfor;
    gdraw p;
    autogrid(,);
    otick.bot(btex $\scriptstyle 1/8$ etex,1/8);
    otick.bot(btex $\scriptstyle 3/8$ etex,3/8);
    otick.bot(btex $\scriptstyle 5/8$ etex,5/8);
    otick.bot(btex $\scriptstyle 7/8$ etex,7/8);
    otick.bot(btex $\scriptstyle 9/8$ etex,9/8);
    otick.lft(btex $\scriptstyle -0.30$ etex,-0.30);
    otick.lft(btex $\scriptstyle -0.20$ etex,-0.20);
    otick.lft(btex $\scriptstyle -0.10$ etex,-0.10);
    otick.lft(btex $\scriptstyle 0.00$ etex,0.0);
    otick.lft(btex $\scriptstyle 0.10$ etex,0.10);
    otick.lft(btex $\scriptstyle 0.20$ etex,0.20);
    otick.lft(btex $\scriptstyle 0.30$ etex,0.30);
  endgraph;
endfig;


beginfig(3);
draw begingraph(3in,2in);
    glabel.lft(btex $u_x$ etex,OUT);
    glabel.bot(btex $t$ etex,OUT);
    setrange(0.125,-0.3,1.125,0.4);
    numeric currentlabeloffset;
    currentlabeloffset := labeloffset;
    labeloffset := 0pt;
    gdata("Feu_ab-initio_1-4.dat",s,
      glabel(closecircle,((scantokens s1)+0.125,(scantokens s2)*6.9));
      )
    gdata("Feu_ab-initio_1-6.dat",s,
      glabel(opencircle,((scantokens s1)+0.125,(scantokens s2)*6.9));
      )
    labeloffset := currentlabeloffset;
    
    vardef f(expr x) =
      numeric result;
      result = 6.9*0.0001*(-6+245*cos(2*pi*(x-0.125))-44*cos(4*pi*(x-0.125))-243*cos(6*pi*(x-0.125))-68*cos(8*pi*(x-0.125)));
      result
    enddef;

    vardef g(expr x) =
      numeric result;
      result := 6.9*0.0001*(184*cos(2*pi*(x-0.125))+21*cos(4*pi*(x-0.125))-64*cos(6*pi*(x-0.125))+22*cos(8*pi*(x-0.125)));
      result
    enddef;

    path p;
    p := (0.1667,f(0.1667))
    for i= 0.1667 step 0.01 until 1.083:
       ..(i,f(i))
    endfor;
    gdraw p dashed evenly;

    path p;
    p := (0.170,g(0.170))
    for i= 0.170 step 0.01 until 1.079:
       ..(i,g(i))
    endfor;
    gdraw p;
    autogrid(,);
    otick.bot(btex $\scriptstyle 1/8$ etex,1/8);
    otick.bot(btex $\scriptstyle 3/8$ etex,3/8);
    otick.bot(btex $\scriptstyle 5/8$ etex,5/8);
    otick.bot(btex $\scriptstyle 7/8$ etex,7/8);
    otick.bot(btex $\scriptstyle 9/8$ etex,9/8);
    otick.lft(btex $\scriptstyle -0.30$ etex,-0.30);
    otick.lft(btex $\scriptstyle -0.20$ etex,-0.20);
    otick.lft(btex $\scriptstyle -0.10$ etex,-0.10);
    otick.lft(btex $\scriptstyle 0.00$ etex,0.0);
    otick.lft(btex $\scriptstyle 0.10$ etex,0.10);
    otick.lft(btex $\scriptstyle 0.20$ etex,0.20);
    otick.lft(btex $\scriptstyle 0.30$ etex,0.30);
    otick.lft(btex $\scriptstyle 0.40$ etex,0.40);
    glabel(btex (a) etex,(5/8,0.3));
  endgraph;
endfig;

beginfig(4);
draw begingraph(3in,2in);
    glabel.lft(btex $u_y$ etex,OUT);
    glabel.bot(btex $t$ etex,OUT);
    setrange(0.125,-0.5,1.125,0.5);
    gdata("Feu_ab-initio_1-4.dat",s,
      glabel(closecircle,((scantokens s1)+0.125,(scantokens s3)*12.0));
      )
    gdata("Feu_ab-initio_1-6.dat",s,
      glabel(opencircle,((scantokens s1)+0.125,(scantokens s3)*12.0));
      )

    vardef f(expr x) =
      numeric result;
      result = 12.0*0.0001*(-80*sin(2*pi*(x-0.125))+228*sin(4*pi*(x-0.125))-60*sin(6*pi*(x-0.125))+0*sin(8*pi*(x-0.125)));
      result
    enddef;

    vardef g(expr x) =
      numeric result;
      result := 12.0*0.0001*(-59*sin(2*pi*(x-0.125))+101*sin(4*pi*(x-0.125))-23*sin(6*pi*(x-0.125)));
      result
    enddef;

    path p;
    p := (0.1667,f(0.1667))
    for i= 0.1667 step 0.01 until 1.083:
       ..(i,f(i))
    endfor;
    gdraw p dashed evenly;

    path p;
    p := (0.170,g(0.170))
    for i= 0.170 step 0.01 until 1.079:
       ..(i,g(i))
    endfor;
    gdraw p;
    autogrid(,);
    otick.bot(btex $\scriptstyle 1/8$ etex,1/8);
    otick.bot(btex $\scriptstyle 3/8$ etex,3/8);
    otick.bot(btex $\scriptstyle 5/8$ etex,5/8);
    otick.bot(btex $\scriptstyle 7/8$ etex,7/8);
    otick.bot(btex $\scriptstyle 9/8$ etex,9/8);
    otick.lft(btex $\scriptstyle -0.50$ etex,-0.50);
    otick.lft(btex $\scriptstyle -0.40$ etex,-0.40);
    otick.lft(btex $\scriptstyle -0.30$ etex,-0.30);
    otick.lft(btex $\scriptstyle -0.20$ etex,-0.20);
    otick.lft(btex $\scriptstyle -0.10$ etex,-0.10);
    otick.lft(btex $\scriptstyle 0.00$ etex,0.0);
    otick.lft(btex $\scriptstyle 0.10$ etex,0.10);
    otick.lft(btex $\scriptstyle 0.20$ etex,0.20);
    otick.lft(btex $\scriptstyle 0.30$ etex,0.30);
    otick.lft(btex $\scriptstyle 0.40$ etex,0.40);
    otick.lft(btex $\scriptstyle 0.50$ etex,0.50);
    glabel(btex (b) etex,(4/8,(6/7)-0.5));
  endgraph;
endfig;

beginfig(5);
draw begingraph(3in,2in);
    glabel.lft(btex $u_z$ etex,OUT);
    glabel.bot(btex $t$ etex,OUT);
    setrange(0.125,-0.2,1.125,0.2);
    gdata("Feu_ab-initio_1-4.dat",s,
      glabel(closecircle,((scantokens s1)+0.125,(scantokens s4)*23.0));
      )
    gdata("Feu_ab-initio_1-6.dat",s,
      glabel(opencircle,((scantokens s1)+0.125,(scantokens s4)*34.5));
      )

    vardef f(expr x) =
      numeric result;
      result = 34.5*0.0001*(-13*sin(2*pi*(x-0.125))-26*sin(4*pi*(x-0.125))-16*sin(6*pi*(x-0.125))+4*sin(8*pi*(x-0.125))-4*sin(10*pi*(x-0.125)));
      result
    enddef;

    vardef g(expr x) =
      numeric result;
      result :=7*0.0001*(-82*sin(2*pi*(x-0.125))-93*sin(4*pi*(x-0.125))-68*sin(6*pi*(x-0.125))-24*sin(8*pi*(x-0.125)));
      result
    enddef;

    path p;
    p := (0.1667,f(0.1667))
    for i= 0.1667 step 0.01 until 1.083:
       ..(i,f(i))
    endfor;
    gdraw p dashed evenly;

    path p;
    p := (0.170,g(0.170))
    for i= 0.170 step 0.01 until 1.079:
       ..(i,g(i))
    endfor;
    gdraw p;
    autogrid(,);
    otick.bot(btex $\scriptstyle 1/8$ etex,1/8);
    otick.bot(btex $\scriptstyle 3/8$ etex,3/8);
    otick.bot(btex $\scriptstyle 5/8$ etex,5/8);
    otick.bot(btex $\scriptstyle 7/8$ etex,7/8);
    otick.bot(btex $\scriptstyle 9/8$ etex,9/8);
    otick.lft(btex $\scriptstyle -0.20$ etex,-0.20);
    otick.lft(btex $\scriptstyle -0.10$ etex,-0.10);
    otick.lft(btex $\scriptstyle 0.00$ etex,0.0);
    otick.lft(btex $\scriptstyle 0.10$ etex,0.10);
    otick.lft(btex $\scriptstyle 0.20$ etex,0.20);
  endgraph;
endfig;
















end


