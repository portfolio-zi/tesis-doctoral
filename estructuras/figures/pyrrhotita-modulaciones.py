#!/usr/bin/python

import matplotlib
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg
from  matplotlib.ticker import MultipleLocator, FormatStrFormatter
from matplotlib.font_manager import FontProperties

from scipy import arange, cos, sin, pi

matplotlib.use('Agg')


Fesinx = [44,-97,6,-4]
Fecosx = [-49,-12,18,-5]
Fesiny = [154,-2,53,1]
Fecosy = [90,18,-25,-5]
Fesinz = [17,115,30,-34]
Fecosz = [-92,-8,78,10]
Fex0 = -16
Fey0 = -8
Fez0 = 0

Ssinx = [9,-3,6,-1]
Scosx = [-5,10,17,-12]
Ssiny = [-17,0,-8,-1]
Scosy = [3,2,6,1]
Ssinz = [26,-146,23,17]
Scosz = [-165,11,38,23]
Sx0 = -0.1671
Sy0 = 0
Sz0 = -0.2624

def modul(coeff0,coeffsin,coeffcos,x):
    co = coeff0
    s1 = coeffsin[0]
    s2 = coeffsin[1]
    s3 = coeffsin[2]
    s4 = coeffsin[3]
    c1 = coeffcos[0]
    c2 = coeffcos[1]
    c3 = coeffcos[2]
    c4 = coeffcos[3]
    result = (co+
              c1*cos(2*pi*x)+s1*sin(2*pi*x)+
              c2*cos(4*pi*x)+s2*sin(4*pi*x)+
              c3*cos(6*pi*x)+s3*sin(6*pi*x)+
              c4*cos(8*pi*x)+s4*sin(8*pi*x)
              )
    return result


x = arange(0,1,0.01)


Fesinx = map(lambda a: a/10000.0,Fesinx)
Fecosx = map(lambda a: a/10000.0,Fecosx)
Fesiny = map(lambda a: a/10000.0,Fesiny)
Fecosy = map(lambda a: a/10000.0,Fecosy)
Fesinz = map(lambda a: a/10000.0,Fesiny)
Fecosz = map(lambda a: a/10000.0,Fecosy)
Fex0 = Fex0/10000.0
Fey0 = -0.0008
Fez0 = 0.0

Fex = modul(Fex0,Fesinx,Fecosx,x)
Fey = modul(Fey0,Fesiny,Fecosx,x)
Fez = modul(Fez0,Fesinz,Fecosx,x)

Ssinx = map(lambda a: a/10000.0,Ssinx)
Scosx = map(lambda a: a/10000.0,Scosx)
Ssiny = map(lambda a: a/10000.0,Ssiny)
Scosy = map(lambda a: a/10000.0,Scosy)
Ssinz = map(lambda a: a/10000.0,Ssiny)
Scosz = map(lambda a: a/10000.0,Scosy)

Sx = modul(Sx0,Ssinx,Scosx,x)
Sy = modul(Sy0,Ssiny,Scosx,x)
Sz = modul(Sz0,Ssinz,Scosx,x)


majorLocator = MultipleLocator(0.2)
majorFormatter = FormatStrFormatter("%3.1f")
minorLocator = MultipleLocator(0.05)


fig = Figure(figsize=(4,4))
ax = fig.add_subplot(111)

ax.set_xlabel(r"$x_4$")
figaxes = fig.gca()
figaxes.set_major_locator=majorLocator
figaxes.set_major_formatter=majorFormatter
figaxes.set_minor_locator=minorLocator
figaxes.set_minor_formatter=FormatStrFormatter("")

#fig.legend(pad = 0.4,shadow=True,prop=FontProperties(size='x-large'))
##ax.axis([0,1,-0.3,0.1])

#title("Pyrrhotita 5.5")

fig.legend(["-","--"],["Fe","S"],"upper right")
ax.set_ylabel(r"$x$")
ax.plot(x, Fex,linestyle="-",color="black",label='Fe')
ax.plot(x, Sx,linestyle="--",color="black",label="S")

# Make the pdf
canvas = FigureCanvasAgg(fig)
# The size * the dpi gives the final image size
#   a4"x4" image * 80 dpi ==> 320x320 pixel image
canvas.print_figure("pyrrhotita-modulaciones-x.eps")


fig = Figure(figsize=(4,4))
ax = fig.add_subplot(111)

fig.legend(["-","--"],["Fe","S"],"upper right")
ax.set_xlabel(r"$x_4$")
figaxes = fig.gca()
figaxes.set_major_locator=majorLocator
figaxes.set_major_formatter=majorFormatter
figaxes.set_minor_locator=minorLocator
figaxes.set_minor_formatter=FormatStrFormatter("")

fig.legend(["-","--"],["Fe","S"],"upper right")
ax.set_ylabel(r"$y$")
ax.plot(x, Fey,linestyle="-",color="black",label='Fe')
ax.plot(x, Sy,linestyle="--",color="black",label="S")

# Make the pdf
canvas = FigureCanvasAgg(fig)
# The size * the dpi gives the final image size
#   a4"x4" image * 80 dpi ==> 320x320 pixel image
canvas.print_figure("pyrrhotita-modulaciones-y.eps")


fig = Figure(figsize=(4,4))
ax = fig.add_subplot(111)

ax.set_xlabel(r"$x_4$")
figaxes = fig.gca()
figaxes.set_major_locator=majorLocator
figaxes.set_major_formatter=majorFormatter
figaxes.set_minor_locator=minorLocator
figaxes.set_minor_formatter=FormatStrFormatter("")
ax.set_ylabel(r"$z$")
ax.plot(x, Fez,linestyle="-",color="black",label='Fe')
ax.plot(x, Sz,linestyle="--",color="black",label="S")

# Make the pdf
canvas = FigureCanvasAgg(fig)
# The size * the dpi gives the final image size
#   a4"x4" image * 80 dpi ==> 320x320 pixel image
canvas.print_figure("pyrrhotita-modulaciones-z.eps")








