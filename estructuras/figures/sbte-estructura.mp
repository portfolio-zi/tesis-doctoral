prologues:=2;
verbatimtex
%&latex
\documentclass{tesis}
\usepackage{tesis}
\begin{document}
etex


beginfig(1);

  u = 1cm;
  
  color sbcolor, tecolor;
  sbcolor = red;
  tecolor = blue;

  atomradii = 3mm;

  alength = 2u;
  structuresep := 3/2alength;
  

  numeric xpos[];

  xdelta = alength/6;
  ydelta = 1/6alength;

  def drawblock(expr point)=
    pair sbpoints[], tepoints[];
    tepoints[0] = point;
    tepoints[1] = point+(3xdelta,0);
    tepoints[2] = tepoints[0]+(-xdelta,2ydelta);
    tepoints[3] = tepoints[1]+(-xdelta,2ydelta);
    tepoints[4] = tepoints[0]+(+xdelta,-2ydelta);
    tepoints[5] = tepoints[1]+(+xdelta,-2ydelta);

    sbpoints[0] = tepoints[0]+(xdelta,ydelta);
    sbpoints[1] = tepoints[0]+(2xdelta,-ydelta);

    draw tepoints[0]--tepoints[1];
    draw tepoints[2]--tepoints[3];
    draw tepoints[4]--tepoints[5];
    draw tepoints[2]--tepoints[4];
    draw tepoints[3]--tepoints[5];

    for i=0 upto 5:
      draw tepoints[i] withpen pencircle scaled atomradii withcolor tecolor;
    endfor;
    
    for i=0 upto 1:
      draw sbpoints[i] withpen pencircle scaled atomradii withcolor sbcolor;
    endfor;
  enddef;

  def drawsblayer(expr point,n)=
    pair sbpoints[];

    sbpoints[0] := point+(0,3ydelta);
    for i=1 upto 2*(n-1):
      m := (i mod 6);
      if m = 0:
	xincr := -1;
      elseif m=1:
	xincr := 0;
      elseif m=2:
	xincr := 1;
      elseif m=3:
	if m<6:
	  xincr := -1;
	else:
	  xincr := 2;
	fi
      elseif m=4:
	xincr := 0;
      else:
	xincr := -2;
      fi
      
      
      sbpoints[i] := sbpoints[0]+ (xincr*xdelta ,(i-1)*ydelta);
      sbpoints[i+2*(n-1)] := sbpoints[i]+(3xdelta,0);
    endfor;
    
    for i=1 upto 2*(n-1):
      draw sbpoints[i] withpen pencircle scaled atomradii withcolor sbcolor;
      draw sbpoints[i+2*(n-1)] withpen pencircle scaled atomradii withcolor sbcolor;
    endfor;
    
  enddef;
  
  def drawestructura(expr point, n)=
    drawblock(point);
    pair newblockpoint;
    drawsblayer(point,n);
    yperiod := 2n+3;
    newblockpoint = point + (-(n mod 3)*xdelta,yperiod*ydelta);
    drawblock(newblockpoint);

    pair unitcell[];
    unitcell[0] := point+(5xdelta,0);
    unitcell[1] := point+(5xdelta,(2n+3)*ydelta);
    drawdblarrow unitcell[0] -- unitcell[1];

    if n mod 3 =0:
      celltoplong := 0.5;
    else:
      celltoplong := 1;
    fi
    
    draw unitcell[0]-(xdelta,0) -- unitcell[0]+(xdelta,0);
    draw unitcell[1]-(xdelta,0) -- unitcell[1]+(celltoplong*xdelta,0);

    pair labelp;
    labelp := point +(2*xdelta,-5ydelta);

    if n=1:
      label(btex \parbox[l]{1cm}{\centering$1$\\ 5(R) \\ \SbTe{2}{3} } etex,labelp);
    elseif n=2:
      label(btex \parbox[l]{1cm}{\centering$2$\\ 7(R) \\ \SbTe{4}{3} } etex,labelp);
    elseif n=3:
      label(btex \parbox[l]{1cm}{\centering$3$\\ 9(P) \\ \SbTe{6}{3} } etex,labelp);
    elseif n=4:
      label(btex \parbox[l]{1cm}{\centering$4$\\ 11(R) \\ \SbTe{8}{3} } etex,labelp);
    elseif n=5:
      label(btex \parbox[l]{1cm}{\centering$5$\\ 13(R) \\ \SbTe{10}{3} } etex,labelp);
    elseif n=6:
      label(btex \parbox[l]{1cm}{\centering$6$\\ 15(P) \\ \SbTe{12}{3} } etex,labelp);
    elseif n=7:
      label(btex \parbox[l]{1cm}{\centering$7$\\ 17(R) \\ \SbTe{14}{3} } etex,labelp);
    elseif n=8:
      label(btex \parbox[l]{1cm}{\centering$8$\\ 19(R) \\ \SbTe{16}{3} } etex,labelp);
    fi
    


  enddef;

  pair structurepoints[];
  structurepoints[0] := (0,0);
  
   for i=1 upto 8:
     structurepoints[i] := structurepoints[i-1]-(structuresep,0);
     drawestructura(structurepoints[i],i);
   endfor;

    
endfig;



end;