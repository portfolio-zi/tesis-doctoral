%!PS
%%BoundingBox: -4 -86 341 86 
%%HiResBoundingBox: -3.5433 -85.28935 340.40741 85.28935 
%%Creator: MetaPost 0.901
%%CreationDate: 2007.06.01:1002
%%Pages: 1
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0 0.5 dtransform truncate idtransform setlinewidth pop [] 0 setdash
 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 0 0 moveto
340.15741 0 lineto stroke
newpath 336.46028 -1.53143 moveto
340.15741 0 lineto
336.46028 1.53143 lineto
 closepath
gsave fill grestore stroke
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 28.34645 -6.01306 moveto
28.34645 6.01306 lineto stroke
newpath 56.6929 -6.01306 moveto
56.6929 6.01306 lineto stroke
newpath 85.03935 -6.01306 moveto
85.03935 6.01306 lineto stroke
newpath 113.3858 -6.01306 moveto
113.3858 6.01306 lineto stroke
newpath 141.73225 -6.01306 moveto
141.73225 6.01306 lineto stroke
newpath 170.0787 -6.01306 moveto
170.0787 6.01306 lineto stroke
newpath 198.42516 -6.01306 moveto
198.42516 6.01306 lineto stroke
newpath 226.7716 -6.01306 moveto
226.7716 6.01306 lineto stroke
newpath 255.11806 -6.01306 moveto
255.11806 6.01306 lineto stroke
newpath 283.46451 -6.01306 moveto
283.46451 6.01306 lineto stroke
newpath 311.81096 -6.01306 moveto
311.81096 6.01306 lineto stroke
newpath 0 -85.03935 moveto
0 85.03935 lineto stroke
 1 0 0 setrgbcolor 0 7.08661 dtransform truncate idtransform setlinewidth pop
newpath 0 0 moveto 0 0 rlineto stroke
newpath 28.34645 0 moveto 0 0 rlineto stroke
newpath 56.6929 0 moveto 0 0 rlineto stroke
newpath 85.03935 0 moveto 0 0 rlineto stroke
newpath 113.3858 0 moveto 0 0 rlineto stroke
newpath 141.73225 0 moveto 0 0 rlineto stroke
newpath 170.0787 0 moveto 0 0 rlineto stroke
newpath 198.42516 0 moveto 0 0 rlineto stroke
newpath 226.7716 0 moveto 0 0 rlineto stroke
newpath 255.11806 0 moveto 0 0 rlineto stroke
newpath 283.46451 0 moveto 0 0 rlineto stroke
newpath 311.81096 0 moveto 0 0 rlineto stroke
showpage
%%EOF
