\chapter{Cambio de base en los grupos superespaciales}
\label{sec:cambio-base}

En el an�lisis estructural superespacial llevado a cabo en este
trabajo no se ha hecho uso, en general, de la base est�ndar que se
utiliza en las Tablas Internacionales de Cristalograf�a
(\citet{janssen1992}) por las razones expuestas en la
secci�n~\ref{seccion:gruposuperespacial}. Se ha preferido utilizar
vectores de modulaci�n sin componentes racionales y paralelos a la
direcci�n rec�proca a la de apilamiento de las capas (paralelo a la
direcci�n $\rec{c}$).

En esta secci�n, en primer lugar, se calcular�n las relaciones que se
establecen entre los elementos de simetr�a expresados en dos bases
diferentes. A continuaci�n, se aplicar�n las expresiones calculadas
para transformar tanto los elementos de simetr�a como las coordenadas
superespaciales de la descripci�n est�ndar a la utilizada en este
trabajo. Se utilizar� como ejemplo el grupo superespacial de la
familia de compuestos de la secci�n~\ref{seccion:formalismo}.
Finalmente, en todos los casos no equivalentes en los que el vector de
modulaci�n tiene una �nica componente irracional y alguna componente
racional, se indicar�n las matrices de cambio a una descripci�n con un
vector de modulaci�n sin componentes racionales.

Supongamos que se ha escogido como base del diagrama de difracci�n de
una estructura en capas la siguiente: $\rec{a}$, $\rec{b}$, $\rec{c}$
y el vector de modulaci�n $\vt{q}$. Cualquier reflexi�n del diagrama
de difracci�n podr� indexarse en esa base de la siguiente forma:
\begin{equation}
  \vt{H}=h\rec{a}+k\rec{b}+\ell\rec{c}+m\vt{q}\equiv\sum_{i=1}^4h_i\rec{a}_i.
\end{equation}
Para simplificar las expresiones, en adelante se utilizar�n los
�ndices $h_i$ y los vectores $\vt{a}_i$ y $\rec{a}_i$ con,
\begin{equation}
  \begin{split}
    h_1&=h\\
    h_2&=k\\
    h_3&=\ell\\
    h_4&=m
  \end{split}\qquad\qquad
  \begin{split}
    \vt{a}_1&=\vt{a}\\
    \vt{a}_2&=\vt{b}\\
    \vt{a}_3&=\vt{c}
  \end{split}\qquad\qquad
  \begin{split}
    \rec{a}_1=&\rec{a}\\
    \rec{a}_2=&\rec{b}\\
    \rec{a}_3=&\rec{c}\\
    \rec{a}_4=&\vt{q}.
  \end{split}
\end{equation}
Las expresiones~(\ref{basedirecta_4D}) y~(\ref{basereciproca_4D})
representan las bases del espacio directo y espacio rec�proco en el
superespacio, respectivamente. Las coordenadas asociadas a la base
directa son $(x_1,x_2,x_3,x_4)$, y los elementos de simetr�a
$\{R|\tau_1,\tau_2,\tau_3,\tau_4\}$, donde $R$ es una matriz
$4\times4$.

Si se utiliza otra base diferente, $\recp{a}_i$ para indexar el
diagrama de difracci�n, relacionada con la anterior seg�n la
expresi�n
\begin{equation}
  \label{apendice_cambio}
  \recp{a}_i=\sum_{j=1}^4M_{ij}\rec{a}_j,
\end{equation}
donde $M_{ij}$ son n�meros racionales que forman una matriz $M$ de
dimensi�n $4\times4$. La relaci�n entre los �ndices de las reflexiones
en ambas bases es,
\begin{equation}
  h_i'=\sum_{j=1}^4M_{ji}^{-1}h_j,
\end{equation}
donde $M^{-1}$ es la matriz inversa de $M$.

La relaci�n entre las coordenadas superespaciales en ambas bases es
\begin{equation}
  x_i'=\sum_{j=1}^4M_{ij}x_j
\end{equation}
y las relaciones entre los elementos de simetr�a son
\begin{equation}
  \label{apendice_elementos}
  \begin{split}
    R_{ij}'&=\sum_{k,\ell=1}^4M_{ik}R_{k\ell}M_{\ell j}^{-1}\\
    \tau_i'&=\sum_{j=1}^4M_{ij}\tau_j.
  \end{split}
\end{equation}


\section{Ejemplo: grupo superespacial
  \boldmath$P\bar{3}1m(\frac{1}{3}\frac{1}{3}\gamma)$}

El grupo superespacial de la familia de compuestos de la
secci�n~\ref{seccion:formalismo} en la base est�ndar es
$P\bar{3}1m(\frac{1}{3}\frac{1}{3}\gamma)$, y los elementos se
incluyen en la tabla~\ref{tab:apendice}. Sin embargo, en la
secci�n~\ref{seccion:formalismo} se ha escogido un vector de
modulaci�n paralelo a $\rec{c}$, y los elementos de simetr�a en esa
descripci�n est�n incluidos en la tabla~\ref{tab:latio_elementos}.  La
matriz de cambio entre el conjunto de vectores $\rec{a}_i$ de la
descripci�n est�ndar y el conjunto de vectores utilizado en la
tabla~\ref{tab:latio_elementos} viene dado por la
ecuaci�n~(\ref{apendice_cambio}), con la siguiente matriz de cambio:
\begin{equation}
  M=
    \begin{pmatrix}
      \frac{1}{3}&\frac{1}{3}&0&0\\
      \frac{2}{3}&-\frac{1}{3}&0&0\\
      0&0&-1&0\\
      -\frac{1}{3}&-\frac{1}{3}&0&-1
    \end{pmatrix}.
\end{equation}
Puede comprobarse que, de acuerdo con la
expresi�n~(\ref{apendice_elementos}), los elementos de simetr�a de la
tabla~\ref{tab:apendice} se transforman en los elementos de la
tabla~\ref{tab:latio_elementos}. En particular, el conjunto de
traslaciones de la red primitiva en la descripci�n est�ndar,
$\{E|n_1,n_2,n_3,n_4\}$, se transforma en el conjunto de traslaciones
siguiente
\begin{equation}
  \{E|\frac{n_1+n_2}{3},\frac{2n_1-n_2}{3},-n_3,-\frac{n_1+n_2}{3}-n_4\},
\end{equation}
que representan una red centrada cuyos elementos de centrado no
equivalentes son los tres incluidos en las dos primeras filas de la
tabla~\ref{tab:latio_elementos}.

\begin{table}
  \centering
  \begin{tabular}{ll}
    \toprule
    $(x_1,x_2,x_3,x_4)$&$(-x_1,-x_2,-x_3,-x_4)$\\
    $(-x_1+x_2,-x_1,x_3,x_1+x_4)$&$(x_1-x_2,x_1,-x_3,-x_1-x_4)$\\
    $(-x_2,x_1-x_2,x_3,x_2+x_4)$&$(x_2,-x_1+x_2,-x_3,-x_2-x_4)$\\
    $(-x_1,-x_1+x_2,x_3,x_1+x_4)$&$(x_1,x_1-x_2,-x_3,-x_1-x_4)$\\
    $(x_2,x_1,x_3,x_4)$&$(-x_2,-x_1,-x_3,-x_4)$\\
    $(x_1-x_2,-x_2,x_3,x_2+x_4)$&$(-x_1+x_2,x_2,-x_3,-x_2-x_4)$\\
    \bottomrule
  \end{tabular}
  \caption[Elementos del grupo superespacial
  $P\bar{3}1m(\frac{1}{3}\frac{1}{3}\gamma)$.]{Elementos del grupo
    superespacial $P\bar{3}1m(\frac{1}{3}\frac{1}{3}\gamma)$. N�tese
    que en la base  elegida la parte rotacional de los elementos de
    simetr�a no es diagonal por bloques $2\times2$, $1\times1$ y
    $1\times1$ como en la representaci�n de la
    secci�n~\ref{seccion:formalismo}.}  
  \label{tab:apendice}
\end{table}

Es importante se�alar que, en la descripci�n est�ndar, los tres
vectores $\vtc{a}_1$, $\vtc{a}_2$ y~$\vtc{a}_3$ de la red directa en
el superespacio, calculados a partir de~(\ref{basereciproca_4D})
y~(\ref{eq:base-reciproca}), tienen componentes a lo largo del espacio
interno; al contrario de lo que ocurre cuando se elige un vector de
modulaci�n sin componentes racionales (ver
relaci�n~(\ref{basedirecta_4D})). Cuando esto ocurre, no es posible
una representaci�n gr�fica tan sencilla como la de la
figura~\ref{figura_latio_super}, y los elementos de simetr�a no son
diagonales por bloques, como puede apreciarse en la
tabla~\ref{tab:apendice}.

En las Tablas Internacionales de Cristalograf�a se presentan los casos
diferentes en los que solamente una de las componentes del vector de
modulaci�n es irracional (elegida siempre paralela al vector
$\rec{c}$) e incluye alguna componente racional. En la
tabla~\ref{apendice_todos_casos} se muestran dichos casos y se
incluyen tambi�n las matrices de cambio $M$ que dan lugar, en cada
caso, a un vector de modulaci�n sin componentes racionales y el
centrado (no est�ndar) a que da lugar la transformaci�n.

En dos de los casos de la tabla~\ref{apendice_todos_casos} el vector
de modulaci�n tiene una componente entera. Esto sucede �nicamente
cuando la red de la estructura promedio es centrada $C$ o centrada $F$
y, por tanto, el vector $\rec{a}$ no pertenece a la red rec�proca.
\newpage

  \begin{longtable}{ll}
    \toprule
    \multicolumn{1}{c}{matriz de cambio}&\multicolumn{1}{c}{centrado no est�ndar}\\
    \multicolumn{1}{c}{utilizada en (\ref{apendice_cambio})}&\\
    \midrule
    \multicolumn{2}{l}{$\vt{q}=(\frac{1}{2}0\gamma)\qquad\text{red
        primitiva}$} \\
    \addlinespace[5pt]
    
%
    $M=\begin{pmatrix}
        \frac{1}{2}&0&0&0\\
        0&1&0&0\\
        0&0&1&0\\
        -\frac{1}{2}&0&0&1
      \end{pmatrix}$&
%
    $\begin{array}{l}\left(x_1,x_2,x_3,x_4\right)\\\left(x_1+\frac{1}{2},x_2,x_3,x_4+\frac{1}{2}\right)\\\end{array}$\\
    \\
%%
    \multicolumn{2}{l}{$\vt{q}=(\frac{1}{2}0\gamma)\qquad\text{centrado A}$}\\
    \addlinespace[5pt]

    $M=\begin{pmatrix}
        \frac{1}{2}&0&0&0\\
        0&1&0&0\\
        0&0&1&0\\
        -\frac{1}{2}&0&0&1
      \end{pmatrix}$&
%
    $\begin{array}{l}\left(x_1,x_2,x_3,x_4\right)\\\left(x_1,x_2+\frac{1}{2},x_3+\frac{1}{2},x_4\right)\\\left(x_1+\frac{1}{2},x_2,x_3,x_4+\frac{1}{2}\right)\\\left(x_1+\frac{1}{2},x_2+\frac{1}{2},x_3+\frac{1}{2},x_4+\frac{1}{2}\right)\\\end{array}$\\
    \\
%%
    \multicolumn{2}{l}{$\vt{q}=(\frac{1}{2}\frac{1}{2}\gamma)\qquad\text{ortorr�mbico}$}\\
    \addlinespace[5pt]

    $M=\begin{pmatrix}
        \frac{1}{2}&0&0&0\\
        0&\frac{1}{2}&0&0\\
        0&0&1&0\\
        -\frac{1}{2}&-\frac{1}{2}&0&1
      \end{pmatrix}$&
%
    $\begin{array}{l}\left(x_1,x_2,x_3,x_4\right)\\\left(x_1+\frac{1}{2},x_2+\frac{1}{2},x_3,x_4\right)\\\left(x_1+\frac{1}{2},x_2,x_3,x_4+\frac{1}{2}\right)\\\left(x_1,x_2+\frac{1}{2},x_3,x_4+\frac{1}{2}\right)\\\end{array}$\\
    \\
%%
    \multicolumn{2}{l}{$\vt{q}=(\frac{1}{2}\frac{1}{2}\gamma)\qquad\text{tetragonal}$}\\
    \addlinespace[5pt]

    $M=\begin{pmatrix}
        \frac{1}{2}&\frac{1}{2}&0&0\\
        -\frac{1}{2}&\frac{1}{2}&0&0\\
        0&0&1&0\\
        -\frac{1}{2}&-\frac{1}{2}&0&1
      \end{pmatrix}$&
%
    $\begin{array}{l}\left(x_1,x_2,x_3,x_4\right)\\\left(x_1+\frac{1}{2},x_2+\frac{1}{2},x_3,x_4+\frac{1}{2}\right)\\\end{array}$\\
    \\
%%
    \multicolumn{2}{l}{$\vt{q}=(10\gamma)\qquad\text{centrado C}$}\\*
    \addlinespace[5pt]
%
    $M=\begin{pmatrix}
        1&0&0&0\\
        0&1&0&0\\
        0&0&1&0\\
        -1&0&0&1
      \end{pmatrix}$&
%
    $\begin{array}{l}\left(x_1,x_2,x_3,x_4\right)\\\left(x_1+\frac{1}{2},x_2+\frac{1}{2},x_3,x_4+\frac{1}{2}\right)\\\end{array}$\\
    \\
%%
    \multicolumn{2}{l}{$\vt{q}=(10\gamma)\qquad\text{centrado F}$}\\
    \addlinespace[5pt]
%
    $M=\begin{pmatrix}
        1&0&0&0\\
        0&1&0&0\\
        0&0&1&0\\
        -1&0&0&1
      \end{pmatrix}$&
%
    $\begin{array}{l}\left(x_1,x_2,x_3,x_4\right)\\\left(x_1+\frac{1}{2},x_2+\frac{1}{2},x_3,x_4+\frac{1}{2}\right)\\\left(x_1+\frac{1}{2},x_2,x_3+\frac{1}{2},x_4+\frac{1}{2}\right)\\\left(x_1,x_2+\frac{1}{2},x_3+\frac{1}{2},x_4\right)\\\end{array}$\\
    \\
%%
    \multicolumn{2}{l}{$\vt{q}=(\frac{1}{3}\frac{1}{3}\gamma)$}\\
    \addlinespace[5pt]
%
    $M=\begin{pmatrix}
        \frac{1}{3}&\frac{1}{3}&0&0\\
        \frac{2}{3}&-\frac{1}{3}&0&0\\
        0&0&-1&0\\
        -\frac{1}{3}&-\frac{1}{3}&0&-1
      \end{pmatrix}$&
%
    $\begin{array}{l}\left(x_1,x_2,x_3,x_4\right)\\\left(x_1+\frac{2}{3},x_2+\frac{1}{3},x_3,x_4+\frac{1}{3}\right)\\\left(x_1+\frac{1}{3},x_2+\frac{2}{3},x_3,x_4+\frac{2}{3}\right)\\\end{array}$\\
%%
    \bottomrule
    \caption[Centrados obtenidos al cambiar de base con un vector de
    modulaci�n sin componentes racionales.]{Matrices de cambio de base
      y centrados obtenidos para los vectores de modulaci�n con una
      �nica componente irracional y alguna racional incluidos en el
      listado de grupos superespaciales en la descripci�n est�ndar
      \citep{janssen1992}, expresados en la base $\rec{a}$, $\rec{b}$
      y~$\rec{c}$. En la columna de la izquierda se incluye una matriz
      de cambio de base que da como resultado un vector de modulaci�n
      sin componentes racionales. En la columna de la derecha se
      incluyen los centrados, no est�ndar, a que da lugar el cambio de
      base. En todos los casos, la parte rotacional de los elementos
      de simetr�a adquiere una forma diagonal por bloques $2\times2$,
      $1\times1$ y~$1\times1$.}
  \label{apendice_todos_casos}
\end{longtable}

