%!PS
%%BoundingBox: -13 -86 341 86 
%%HiResBoundingBox: -12.80252 -85.28935 340.40741 85.28935 
%%Creator: MetaPost 0.901
%%CreationDate: 2007.06.01:1002
%%Pages: 1
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0 0.5 dtransform truncate idtransform setlinewidth pop [] 0 setdash
 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 0 0 moveto
340.15741 0 lineto stroke
newpath 336.46028 -1.53143 moveto
340.15741 0 lineto
336.46028 1.53143 lineto
 closepath
gsave fill grestore stroke
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 28.34645 -6.01306 moveto
28.34645 6.01306 lineto stroke
newpath 56.6929 -6.01306 moveto
56.6929 6.01306 lineto stroke
newpath 85.03935 -6.01306 moveto
85.03935 6.01306 lineto stroke
newpath 113.3858 -6.01306 moveto
113.3858 6.01306 lineto stroke
newpath 141.73225 -6.01306 moveto
141.73225 6.01306 lineto stroke
newpath 170.0787 -6.01306 moveto
170.0787 6.01306 lineto stroke
newpath 198.42516 -6.01306 moveto
198.42516 6.01306 lineto stroke
newpath 226.7716 -6.01306 moveto
226.7716 6.01306 lineto stroke
newpath 255.11806 -6.01306 moveto
255.11806 6.01306 lineto stroke
newpath 283.46451 -6.01306 moveto
283.46451 6.01306 lineto stroke
newpath 311.81096 -6.01306 moveto
311.81096 6.01306 lineto stroke
newpath 0 -85.03935 moveto
0 85.03935 lineto stroke
newpath 0 -85.03935 moveto
0 85.03935 lineto stroke
newpath 28.34645 -85.03935 moveto
28.34645 85.03935 lineto stroke
newpath 56.6929 -85.03935 moveto
56.6929 85.03935 lineto stroke
newpath 85.03935 -85.03935 moveto
85.03935 85.03935 lineto stroke
newpath 113.3858 -85.03935 moveto
113.3858 85.03935 lineto stroke
newpath 141.73225 -85.03935 moveto
141.73225 85.03935 lineto stroke
newpath 170.0787 -85.03935 moveto
170.0787 85.03935 lineto stroke
newpath 198.42516 -85.03935 moveto
198.42516 85.03935 lineto stroke
newpath 226.7716 -85.03935 moveto
226.7716 85.03935 lineto stroke
newpath 255.11806 -85.03935 moveto
255.11806 85.03935 lineto stroke
newpath 283.46451 -85.03935 moveto
283.46451 85.03935 lineto stroke
newpath 311.81096 -85.03935 moveto
311.81096 85.03935 lineto stroke
gsave newpath 0 -85.03935 moveto
311.81096 -85.03935 lineto
311.81096 85.03935 lineto
0 85.03935 lineto
 closepath clip
 0 0 dtransform truncate idtransform setlinewidth pop
newpath 0 -396.85031 moveto
311.81096 -595.27676 lineto stroke
newpath 0 -368.50386 moveto
311.81096 -566.93031 lineto stroke
newpath 0 -340.15741 moveto
311.81096 -538.58386 lineto stroke
newpath 0 -311.81096 moveto
311.81096 -510.23741 lineto stroke
newpath 0 -283.46451 moveto
311.81096 -481.89096 lineto stroke
newpath 0 -255.11806 moveto
311.81096 -453.54451 lineto stroke
newpath 0 -226.7716 moveto
311.81096 -425.19806 lineto stroke
newpath 0 -198.42516 moveto
311.81096 -396.85161 lineto stroke
newpath 0 -170.0787 moveto
311.81096 -368.50516 lineto stroke
newpath 0 -141.73225 moveto
311.81096 -340.1587 lineto stroke
newpath 0 -113.3858 moveto
311.81096 -311.81226 lineto stroke
newpath 0 -85.03935 moveto
311.81096 -283.4658 lineto stroke
newpath 0 -56.6929 moveto
311.81096 -255.11935 lineto stroke
newpath 0 -28.34645 moveto
311.81096 -226.7729 lineto stroke
newpath 0 0 moveto
311.81096 -198.42645 lineto stroke
newpath 0 28.34645 moveto
311.81096 -170.08 lineto stroke
newpath 0 56.6929 moveto
311.81096 -141.73355 lineto stroke
newpath 0 85.03935 moveto
311.81096 -113.3871 lineto stroke
newpath 0 113.3858 moveto
311.81096 -85.04065 lineto stroke
newpath 0 141.73225 moveto
311.81096 -56.6942 lineto stroke
newpath 0 170.0787 moveto
311.81096 -28.34775 lineto stroke
newpath 0 198.42516 moveto
311.81096 -0.0013 lineto stroke
newpath 0 226.7716 moveto
311.81096 28.34515 lineto stroke
newpath 0 255.11806 moveto
311.81096 56.6916 lineto stroke
newpath 0 283.46451 moveto
311.81096 85.03806 lineto stroke
newpath 0 311.81096 moveto
311.81096 113.3845 lineto stroke
newpath 0 340.15741 moveto
311.81096 141.73096 lineto stroke
newpath 0 368.50386 moveto
311.81096 170.07741 lineto stroke
newpath 0 396.85031 moveto
311.81096 198.42386 lineto stroke
grestore
 1 0 0 setrgbcolor 0 7.08661 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath 0 0 moveto 0 0 rlineto stroke
newpath 21.2767 0 moveto 0 0 rlineto stroke
newpath 65.95212 0 moveto 0 0 rlineto stroke
newpath 79.98262 0 moveto 0 0 rlineto stroke
newpath 110.74951 0 moveto 0 0 rlineto stroke
newpath 150.2419 0 moveto 0 0 rlineto stroke
newpath 161.57036 0 moveto 0 0 rlineto stroke
newpath 201.05885 0 moveto 0 0 rlineto stroke
newpath 231.8305 0 moveto 0 0 rlineto stroke
newpath 245.8584 0 moveto 0 0 rlineto stroke
newpath 290.53252 0 moveto 0 0 rlineto stroke
newpath 311.81355 0 moveto 0 0 rlineto stroke
newpath 0 0 moveto 0 0 rlineto stroke
newpath 5.05717 2.57703 moveto 0 0 rlineto stroke
newpath 8.50922 5.15405 moveto 0 0 rlineto stroke
newpath 9.25922 7.73065 moveto 0 0 rlineto stroke
newpath 7.06975 10.30768 moveto 0 0 rlineto stroke
newpath 2.63542 12.8847 moveto 0 0 rlineto stroke
newpath -2.63585 15.46175 moveto 0 0 rlineto stroke
newpath -7.06975 18.03877 moveto 0 0 rlineto stroke
newpath -9.25922 20.6158 moveto 0 0 rlineto stroke
newpath -8.50877 23.1924 moveto 0 0 rlineto stroke
newpath -5.05717 25.76942 moveto 0 0 rlineto stroke
newpath 0.00043 28.34645 moveto 0 0 rlineto stroke
showpage
%%EOF
