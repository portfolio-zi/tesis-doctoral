TEXFILES = tesis.cls tesis.sty tesis.bst tesis.tex tesis.bib Makefile  ./difractometro/disenodifractometro.tex ./difractometro/disenointerfaz.tex ./difractometro/medidas.tex ./difractometro/velocidades.tex ./difractometro/calibrar-detector.tex ./estructuras/pyrrhotite.tex ./estructuras/sbte.tex

FIGUREDIR = .:./difractometro/figures:./superspacio/figures:./estructuras/figures:./presentacion/figuras:

CLASS = tesis


ifdef FINAL
DRAFTOPTION = final
else
DRAFTOPTION = draft
endif


class:	./doc/tesis.dtx ./doc/tesis.ins
	cd doc && ${MAKE} tesis.cls
	cp doc/tesis.cls .

classdoc: ./doc/tesis.dtx
	cd doc && ${MAKE} tesisclass.pdf
	cp doc/tesisclass.pdf .

classall: class classdoc



all: class pdf


pdf: tesis.pdf


one: ${TEXFILES} ${CLASSFILE} ${MPFIGURES} ${BIBTEXFILES} class
	make figures
	pdflatex "\PassOptionsToClass{$(DRAFTOPTION)}{$(CLASS)}\input{tesis}"

TEXINPUTS := ${FIGUREDIR}:${TEXINPUTS}
.EXPORT: TEXINPUTS

tesis.pdf: $(TEXFILES) ${CLASSFILE} ${MPFIGURES} ${BIBTEXFILES} class
	echo $(TEXINPUTS)
	echo $(DRAFTOPTION)
	make figures
	pdflatex "\PassOptionsToClass{$(DRAFTOPTION)}{$(CLASS)}\input{tesis}"
	bibtex tesis
#	bibtex tesis.gls
	pdflatex "\PassOptionsToClass{$(DRAFTOPTION)}{$(CLASS)}\input{tesis}"
	pdflatex "\PassOptionsToClass{$(DRAFTOPTION)}{$(CLASS)}\input{tesis}"


figures: 
	cd difractometro/figures/ && ${MAKE} all
	cd superspacio/figures/ && ${MAKE} all
	cd estructuras/figures/ && ${MAKE} all

draft:
	make DRAFTOPTION=draft pdf
	mv tesis.pdf tesis-draft.pdf

final:
	make DRAFTOPTION=final pdf

tesis:
	make final


clean:
	make cleanmp
	rm -f *.aux
	rm -f *.dvi
	rm -f *.log
	rm -f *.bbl
	rm -f *.blg
	rm -f *.toc
	rm -f *.log
	rm -f *.lof
	rm -f *.lot
	rm -f *.lox
	rm -f *.mpx
	rm -f *.mp
	rm -f *.[0-9]*
	rm -f *.pdf
	rm -f *~

cleanmp:
	cd difractometro/figures/; ${MAKE} clean
	cd estructuras/figures/; ${MAKE} clean
	cd superspacio/figures; ${MAKE} clean

pres: ${MPFIGURES}
	make figures
	pdflatex presentacion
